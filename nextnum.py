import bisect
import random


class RandomGen(object):
    def __init__(self, values, weights, seed=None):
        self.values = values
        total = 0
        weight_map = []
        for w in weights:
            total += w
            weight_map.append(total)
        self.weight_map = weight_map
        self.total = total
        if seed is not None:
            random.seed(seed)

    def next_num(self):
        cut = random.random() * self.total
        index = bisect.bisect_right(self.weight_map, cut)
        return self.values[index]


def randomgen_factory(values, weights, seed=None):
    """
    More pythonic... as a generator?
    """
    total = 0
    weight_map = []
    for w in weights:
        total += w
        weight_map.append(total)
    if seed is not None:
        random.seed(seed)

    def randgen(count):
        for i in xrange(count):
            cut = random.random() * total
            index = bisect.bisect_right(weight_map, cut)
            yield values[index]

    return randgen
