import unittest
from collections import Counter

from nextnum import RandomGen, randomgen_factory


class TestRandomGenerators(unittest.TestCase):
    def setUp(self):
        self.values = [-2, -1, 0, 1, 2]
        self.weights = [0.01, 0.2, 0.58, 0.2, 0.01]
        self.seed = 5
        self.result = Counter({0: 582, -1: 209, 1: 193, -2: 11, 2: 5})

    def test_class_total(self):
        randgen = RandomGen(self.values, self.weights, seed=self.seed)
        assert randgen.total == sum(self.weights)

    def test_class_nextnum(self):
        randgen = RandomGen(self.values, self.weights, seed=self.seed)
        result = Counter(randgen.next_num() for i in range(1000))
        assert result == self.result

    def test_generator(self):
        randgen = randomgen_factory(self.values, self.weights, seed=self.seed)
        result = Counter(randgen(1000))
        assert result == self.result


if __name__ == '__main__':
    unittest.main()
