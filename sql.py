import sqlite3


def main():
    conn = sqlite3.connect('quant.sqlite')
    cur = conn.cursor()

    query = """
    SELECT
      product.*,
      o.total
    FROM
      product
    INNER JOIN
      (
        SELECT
          product_id,
          SUM(quantity) AS total
        FROM orders
        GROUP BY product_id
        HAVING total < 10
      )
      AS o
      ON o.product_id = product.product_id
    WHERE
      product.available_from < date('now', '-1 month')
    """
    cur.execute(query)

    row_format = "{:>10}  {:<30} {:>6} {:>16} {:>6}"
    print row_format.format(*[col[0] for col in cur.description])
    for row in cur:
        print row_format.format(*row)

    conn.close()


if __name__ == '__main__':
    main()
